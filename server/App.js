const express = require("express");
const dotenv = require("dotenv");
const cors = require("cors");
const axios = require("axios").default;
const app = express();
const port = 8000;

const { OAuth2Client } = require("google-auth-library");
const { filterCars } = require("./Filter");

app.use(express.json());
app.use(cors());

dotenv.config();
const client = new OAuth2Client(process.env.REACT_APP_GOOGLE_CLIENT_ID);

app.get("/", (req, res) => {
    res.send({
        message: "Api Success",
    });
});

//Google OAuth
const users = [];
function upsert(array, item) {
    const i = array.findIndex((_item) => _item.email === item.email);
    if (i > -1) array[i] = item;
    else array.push(item);
}

app.post("/api/google-login", async (req, res) => {
    const { token } = req.body;
    const ticket = await client.verifyIdToken({
        idToken: token,
        audience: process.env.CLIENT_ID,
    });
    const { name, email, picture } = ticket.getPayload();
    upsert(users, { name, email, picture });
    res.status(201);
    res.json({ name, email, picture });
});

app.post("/api/cars", async (req, res) => {
    const cars = (await axios.get("https://raw.githubusercontent.com/fnurhidayat/probable-garbanzo/main/data/cars.min.json")).data;
    const filteredCars = filterCars(cars, req.body);
    res.send(filteredCars);
});

app.listen(port, () => {
    console.log(`Server started at http://localhost:${port}`);
});
